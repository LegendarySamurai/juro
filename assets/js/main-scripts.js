/***
===========================
Main JavaScripts
===========================
***/

$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:35,
        nav:true,
        navText: false,
        slideBy: 3,
        responsive:{
            0:{
                items:3
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })
});